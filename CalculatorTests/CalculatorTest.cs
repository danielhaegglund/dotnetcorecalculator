using Xunit;
using CalculatorLib;
using FluentAssertions;

namespace CalculatorTests
{
    public class CalculatorTest
    {
        [Theory]
        [InlineData(1,1,2)]
        [InlineData(2,2,4)]
        [InlineData(1,2,3)]
        [InlineData(2,1,3)]
        [InlineData(5,6,11)]
        [InlineData(100,116,216)]
        [InlineData(0,0,0)]
        public void AdditionTheory(int operand1, int operand2, long sum)
        {
            var calculator = new Calculator();
            var result = calculator.Add(operand1, operand2);
            result.Should().Be(sum);
        }
    }
}